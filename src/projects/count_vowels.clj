(ns projects.count-vowels)

(defn vowel? [c]
  "Test if a character is a vowel"
	(contains? #{\a \e \i \o \u} c))

(defn get-vowels [s]
  "Generate a seq of vowels in the given string"
	(filter vowel? s))

(defn count-vowels [s]
  "Return the count of all vowels in the string"
	(count (get-vowels s)))

(defn dict-inc [m value]
  "Increment a count for the given key in the hashmap"
  (update-in m [value] (fnil inc 0)))

(defn get-sums [m s]
  "Return a hashmap of vowel counts in the given string"
  (if (empty? s) m
    (get-sums 
      (if (vowel? (first s)) 
        (dict-inc m (first s))
        m)
      (rest s))))

(defn do-count-vowels [s]
  (println (str "Count: " (count-vowels s)))
  (println (get-sums {} s)))
