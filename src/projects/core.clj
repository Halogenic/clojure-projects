(ns projects.core
  (:require [projects.factorial :as module])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (module/fact (Integer/parseInt (first args)))))
