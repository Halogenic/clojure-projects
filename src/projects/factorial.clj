(ns projects.factorial)

(defn fact [n]
  (if (zero? n)
    1
    (reduce * (range 1 (inc n)))))
