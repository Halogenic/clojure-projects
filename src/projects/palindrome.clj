(ns projects.palindrome
  (:require [clojure.string :as string]))

(defn palindrome? [s] 
  (= s (string/reverse s)))

(defn do-palindrome [s]
  (println (palindrome? s)))
